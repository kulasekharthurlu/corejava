package com.app.varables;

class x {
}

class Emp {
}

class Y {
}

class Student {
}

class Test7 {
    static void m2(Y y, Student s) {
        System.out.println("m2 method");
    }

    public static void main(String[] args) {
        Test7 t = new Test7();
        x x = new x();
        Emp e = new Emp();
        t.m1(x, e);

        Y y = new Y();
        Student s = new Student();
        Test7.m2(y, s);
    }

    void m1(x x, Emp e) {
        System.out.println("m1 method");
    }
}



