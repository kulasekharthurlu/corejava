package com.app.varables;

public class Return {
    int m1() {
        System.out.println("m1 method");
        return 12;
    }

    float m2() {
        System.out.println("m2 methode");
        return 10.5f;//
    }

    static char m3() {
        System.out.println("m3 method");
        return 'f';
    }

    public static void main(String[] args) {
        Return r = new Return();
        int d = r.m1();
        System.out.println("return value of m1()=" + d);
        float f = r.m2();
        System.out.println("return value of m1()=" + f);
        char ch = Return.m3();
        System.out.println("return value of m1()=" + ch);
    }
}
