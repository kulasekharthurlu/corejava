package com.app.methods;

public class Emp {
    int eid;
    String ename;
    float esal;
    Emp(int eid,String ename,float esal)
    {
        this.eid=eid;
        this.ename=ename;
        this.esal=esal;
    }
    void disp()
    {
        System.out.println("emp id="+eid);
        System.out.println("emp id="+ename);
        System.out.println("emp id="+esal);
    }

    public static void main(String[] args) {
        Emp e1=new   Emp(111,"ratan",2334);
        e1.disp();
        Emp e2=new   Emp(222,"anu",888);
        e2.disp();
    }
}
