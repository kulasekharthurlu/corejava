package com.app.methods;

import com.app.varables.Test;

public class Tony {
    Tony()
    {
        this(78);
        System.out.println("0-org cons");
    }
    Tony(int a)
    {
        this(78,89);
        System.out.println("1-org cons");
    }
    Tony(int a,int b)
    {
        System.out.println("2-org cons");
    }
    public static void main(String[] args) {
        Tony t=new Tony();
    }


}
