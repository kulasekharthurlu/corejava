package com.app.patterns;

import com.app.Emp;

import java.util.Arrays;

public class Arry2 {
    static class Test {
        public static void main(String[] args) {
            Emp e1 = new Emp(111, "k");
            Emp e2 = new Emp(222, "h");
            Emp e3 = new Emp(888, "y");
            Emp[] e = new Emp[3];
            e[0] = e1;
            e[1] = e2;
            e[2] = e3;
            for (Emp ee : e) {
                System.out.println(ee.getId() + " " + ee.getName());
            }
            Arrays.stream(e).forEach(s-> System.out.println(s.getId()+" "+s.getName()));
        }
    }
}


