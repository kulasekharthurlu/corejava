package com.app.patterns;

public class This {
    This m1()
    {
        System.out.println("m1 methode");
        This t=new This();
        return t;
    }
    This m2()
    {
        System.out.println("m2 methode");
        return this;
    }
    public static void main(String[] args) {
        This t=new This();
        This t2=t.m1();
        This t3= t.m1();
    }
}
