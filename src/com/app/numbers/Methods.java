package com.app.numbers;

class Test {
    void checkingForPrime(int number){
        boolean isPrime=true;
        for (int i = 2; i < number; i++) {
            if(number%i==0)
                isPrime=false;
        }
        if(isPrime){
            System.out.println(number+" is prime");
        }else {
            System.out.println(number+" is not prime");
        }

    }

    public static void main(String[] args) {
        int n=12;
        new Test().checkingForPrime(n);

    }

}
