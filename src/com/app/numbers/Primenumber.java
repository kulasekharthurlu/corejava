package com.app.numbers;

import java.util.Scanner;

public class Primenumber {
    public static void main(String[] args) {
        System.out.println("Enter a number");
        Scanner sc=new Scanner(System.in);
        int i=sc.nextInt();
        boolean isPrime=true;
        for (int j = 2; j <i ; j++) {
            if(i%j==0){
                isPrime=false;
            }
        }
        if (isPrime)
            System.out.println(i+" is prime");
        else
            System.out.println(i+" is not prime");

    }
}
